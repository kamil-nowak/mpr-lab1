package pl.edu.pjatk.mpr.user.db.repos;

import java.util.List;

import pl.edu.pjatk.mpr.user.module.Permission;

public interface IPermissionRepository extends IRepository<Permission>{

	public List<Permission> withName(String permissionName);
	public List<Permission> withName(int permissionId);
}