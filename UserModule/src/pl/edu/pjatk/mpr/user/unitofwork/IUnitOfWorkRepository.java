package pl.edu.pjatk.mpr.user.unitofwork;

import pl.edu.pjatk.mpr.user.module.Entity;

public interface IUnitOfWorkRepository {

	public void persistAdd(Entity entity);
	public void persistUpdate(Entity entity);
	public void persistDelete(Entity entity);
}