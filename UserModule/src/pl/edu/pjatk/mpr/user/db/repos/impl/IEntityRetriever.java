package pl.edu.pjatk.mpr.user.db.repos.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import pl.edu.pjatk.mpr.user.module.Entity;

public interface IEntityRetriever<TEntity extends Entity> {

	public TEntity build(ResultSet rs) throws SQLException;
	
}