package pl.edu.pjatk.mpr.user.db.repos;

import java.util.List;

import pl.edu.pjatk.mpr.user.module.EnumerationValue;

public interface IEnumerationValueRepository extends IRepository<EnumerationValue> {
	
	public List<EnumerationValue> withName(String name);
	public List<EnumerationValue> withValue(String value);
	public List<EnumerationValue> withStringKey(String key);

}