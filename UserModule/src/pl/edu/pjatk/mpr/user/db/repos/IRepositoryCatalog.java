package pl.edu.pjatk.mpr.user.db.repos;

import pl.edu.pjatk.mpr.user.module.EnumerationValue;
import pl.edu.pjatk.mpr.user.module.Person;
import pl.edu.pjatk.mpr.user.module.User;

public interface IRepositoryCatalog {

	public IUserRepository getUsers();
	public IRepository<User> getUser();
	public IRepository<Person> getPerson();
	public IRepository<EnumerationValue> getEnumerationValue();
	public void commit();
}