package pl.edu.pjatk.mpr.user.db.repos;

import java.util.List;

import pl.edu.pjatk.mpr.user.module.Address;

public interface IAddressRepository extends IRepository<Address>{
	public List<Address> withCountry(String country);
	public List<Address> withCity(String city);
	public List<Address> withStreet(String street);
}