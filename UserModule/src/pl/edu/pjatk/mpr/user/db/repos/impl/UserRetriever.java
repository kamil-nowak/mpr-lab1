package pl.edu.pjatk.mpr.user.db.repos.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import pl.edu.pjatk.mpr.user.module.User;

public class UserRetriever implements IEntityRetriever<User> {

	@Override
	public User build(ResultSet rs) throws SQLException {
		User result = new User();
		result.setId(rs.getInt("id"));
		result.setUsername(rs.getString("username"));
		result.setPassword(rs.getString("password"));
		return result;
	}

}
