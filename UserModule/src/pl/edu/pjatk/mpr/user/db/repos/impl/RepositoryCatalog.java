package pl.edu.pjatk.mpr.user.db.repos.impl;

import java.sql.Connection;

import pl.edu.pjatk.mpr.user.db.repos.*;
import pl.edu.pjatk.mpr.user.module.*;
import pl.edu.pjatk.mpr.user.unitofwork.IUnitOfWork;


public class RepositoryCatalog implements IRepositoryCatalog{

	private Connection connection;
	private IUnitOfWork uow;
	
	public RepositoryCatalog(Connection connection, IUnitOfWork uow) {
		super();
		this.connection = connection;
		this.uow = uow;
	}

	@Override
	public IUserRepository getUsers() {
		return new UserRepository(connection, new UserRetriever(), uow);
	}


	@Override
	public void commit() {
		uow.commit();
	}

	@Override
	public IRepository<EnumerationValue> getEnumerationValue() {

		return new EnumerationValueRepository(connection, 
				new EnumerationValueRetriever(), uow);
	}

	@Override
	
	public IRepository<User> getUser() {
		return new UserRepository(connection,
				new UserRetriever(), uow);
	}

	@Override
	public IRepository<Person> getPerson() {
		// TODO Auto-generated method stub
		return null;
	}

}
