package pl.edu.pjatk.mpr.user.db.repos;

import java.util.List;

import pl.edu.pjatk.mpr.user.module.Person;

public interface IPersonRepository extends IRepository<Person>{

	public List<Person> withName(String personName);
	public List<Person> withSurname(String personSurname);
	public List<Person> withName(int personId);
}