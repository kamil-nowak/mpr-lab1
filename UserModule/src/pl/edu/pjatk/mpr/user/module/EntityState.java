package pl.edu.pjatk.mpr.user.module;

public enum EntityState {
	New, Changed, Unchanged, Deleted
}