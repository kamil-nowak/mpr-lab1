package pl.edu.pjatk.mpr.user.db.repos.impl;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import pl.edu.pjatk.mpr.user.db.repos.IRepositoryCatalog;
import pl.edu.pjatk.mpr.user.unitofwork.*;

public class RepositoryCatalogProvider {


	private static String url="jdbc:hsqldb:hsql://localhost/workdb";
	
	public static IRepositoryCatalog catalog()
	{

		try {
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new UnitOfWork(connection);
			IRepositoryCatalog catalog = new RepositoryCatalog(connection, uow);
		
			return catalog;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
