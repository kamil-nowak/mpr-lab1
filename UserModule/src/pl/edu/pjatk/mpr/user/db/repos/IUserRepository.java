package pl.edu.pjatk.mpr.user.db.repos;

import java.util.List;

import pl.edu.pjatk.mpr.user.module.User;

public interface IUserRepository extends IRepository<User>{

	public List<User> withUsername(String username);
	public List<User> withUsername(int userId);
}